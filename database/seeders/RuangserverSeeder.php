<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RuangserverSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ruang_server')->insert([[
            'user_id' => '2',
            'no_ktp' => '1234567890987654',
            'tanggal' => '2023-03-19',
            'waktu_datang' => '20:45:34',
            'waktu_meninggalkan' => '20:54:34',
            'no_kartu_akses' => 'RS-2023041100001',
            'no_rak' => '3',
            'aktivitas' => 'Pemasangan',
            'izin_foto' => 'Ya',
            'status' => '2',
            'catatan' => 'error laborum perferendis repudiandae unde ut mollitia officiis. Quas ipsa at tempora cum accusantium molestias, nihil ad animi distinctio doloribus rem eius exercitationem. Explicabo illum quasi rem veritatis quos, tenetur omnis.',
            'created_at' => '2023-03-10 12:13:00'
        ],
        [
            'user_id' => '2',
            'no_ktp' => '1234567890987654',
            'tanggal' => '2023-03-18',
            'waktu_datang' => '20:45:34',
            'waktu_meninggalkan' => '20:54:34',
            'no_kartu_akses' => 'RS-2023041200002',
            'no_rak' => '13',
            'aktivitas' => 'Pemasangan',
            'izin_foto' => 'Ya',
            'status' => '2',
            'catatan' => 'error laborum perferendis repudiandae unde ut mollitia officiis. Quas ipsa at tempora cum accusantium molestias, nihil ad animi distinctio doloribus rem eius exercitationem. Explicabo illum quasi rem veritatis quos, tenetur omnis.',
            'created_at' => '2023-03-11 12:13:00'
        ],
        [
            'user_id' => '2',
            'no_ktp' => '1234567890987654',
            'tanggal' => '2023-03-18',
            'waktu_datang' => '20:45:34',
            'waktu_meninggalkan' => '20:54:34',
            'no_kartu_akses' => '',
            'no_rak' => '13',
            'aktivitas' => 'Pemasangan',
            'izin_foto' => 'Ya',
            'status' => '1',
            'catatan' => 'error laborum perferendis repudiandae unde ut mollitia officiis. Quas ipsa at tempora cum accusantium molestias, nihil ad animi distinctio doloribus rem eius exercitationem. Explicabo illum quasi rem veritatis quos, tenetur omnis.',
            'created_at' => '2023-04-28 12:13:00'
        ]]);

        DB::table('pemohon')->insert([
            [
                'nama' => 'Edith',
                'perusahaan' => 'Telkom',
                'jabatan' => 'programmer',
                'ruang_server_id' => '1',
            ],
            [
                'nama' => 'Estes',
                'perusahaan' => 'Shoppe',
                'jabatan' => 'sekertaris',
                'ruang_server_id' => '1',
            ],
            [
                'nama' => 'Hilda',
                'perusahaan' => 'Tokopedia',
                'jabatan' => 'bendahara',
                'ruang_server_id' => '1',
            ],
        ],
        [
            [
                'nama' => 'Edith',
                'perusahaan' => 'Telkom',
                'jabatan' => 'programmer',
                'ruang_server_id' => '2',
            ],
            [
                'nama' => 'Estes',
                'perusahaan' => 'Shoppe',
                'jabatan' => 'sekertaris',
                'ruang_server_id' => '2',
            ],
            [
                'nama' => 'Hilda',
                'perusahaan' => 'Tokopedia',
                'jabatan' => 'bendahara',
                'ruang_server_id' => '2',
            ],
        ],
        [
            [
                'nama' => 'Edith',
                'perusahaan' => 'Telkom',
                'jabatan' => 'programmer',
                'ruang_server_id' => '3',
            ],
            [
                'nama' => 'Estes',
                'perusahaan' => 'Shoppe',
                'jabatan' => 'sekertaris',
                'ruang_server_id' => '3',
            ],
            [
                'nama' => 'Hilda',
                'perusahaan' => 'Tokopedia',
                'jabatan' => 'bendahara',
                'ruang_server_id' => '3',
            ],
        ]);
        }
}
