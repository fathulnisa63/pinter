<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DevwebSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dev_web')->insert([
            [
                'nama' => 'Salsabila',
                'nip' => '123456789',
                'jabatan' => 'programmer',
                'email' => 'salsabil@gmail.com',
                'phone' => '081390199123',
                'hosting' => '1'
    
            ],
        ]);
    }
}
