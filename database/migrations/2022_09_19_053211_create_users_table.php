<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('nip')->unique();
            $table->string('nama');
            $table->string('email');
            // $table->string('username');
            $table->string('password');
            $table->string('telp');
            $table->string('jabatan');
            $table->string('foto');
            $table->foreignId('opd_kode')->nullable()->constrained('opd')->onUpdate('cascade')->nullOnDelete();
            $table->foreignId('role_id')->nullable()->constrained('role')->cascadeOnUpdate()->nullOnDelete();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
