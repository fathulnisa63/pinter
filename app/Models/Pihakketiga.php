<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pihakketiga extends Model
{
    use HasFactory;
    protected $table = 'pihak_ketiga';
    protected $guarded = ['id'];

    public function hakakses()
    {
        return $this->hasMany(Hakakses::class);
    }
}
