<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hakakses extends Model
{
    use HasFactory;
    protected $table = 'hak_akses';
    protected $guarded = ['id'];

    public function pihakketiga(){
        return $this->belongsTo(Pihakketiga::class, 'pihak_ketiga');
    }

    public function jenisakses(){
        return $this->belongsTo(Jenishak::class, 'jenis_hak_akses');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function statusakses(){
        return $this->belongsTo(Status::class, 'status');
    }

    public static function generateNomor()
    {
        $tahun = date('Y');
        $bulan = date('m');
        $tanggal = date('d');
        $lastNoKar = self::where('no_hak_akses', 'like', '%'.$tahun.'%')->max('no_hak_akses');
        if ($lastNoKar) {
            $lastTahun = substr($lastNoKar, 3, 4);
            if ($lastTahun == $tahun) {
                $noUrut = intval(substr($lastNoKar, -5)) + 1;
            } else {
                $noUrut = 1;
            }
        } else {
            $noUrut = 1;
        }
        $noKar = 'HA-'. $tahun . $bulan . $tanggal . sprintf('%05d', $noUrut);
        return $noKar;
    }
}
