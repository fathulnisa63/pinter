<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Status;
use App\Models\Pemohon;
use App\Models\Ruangserver;
use Illuminate\Http\Request;
use Elibyy\TCPDF\Facades\TCPDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class RuangController extends Controller
{
    public function index(Request $request)
    {
        if ($request->tahun) {
            if (Auth::user()->role_id == 2) {
                $rserver = Ruangserver::where('user_id', Auth::user()->id)->where(DB::raw('YEAR(tanggal)'), $request->tahun)->latest()->get();
            } else {
                $rserver = Ruangserver::where(DB::raw('YEAR(tanggal)'), $request->tahun)->latest()->get();
            }
        } else {
            if (Auth::user()->role_id == 2) {
                $rserver = Ruangserver::where('user_id', Auth::user()->id)->where(DB::raw('YEAR(tanggal)'), now())->latest()->get();
            } else {
                $rserver = Ruangserver::where(DB::raw('YEAR(tanggal)'), now())->latest()->get();
            }
        }
        
        $tahun = DB::table('ruang_server')->select(DB::raw('YEAR(tanggal) as tahun'))->orderBy(DB::raw('YEAR(tanggal)'), 'desc')->groupBy(DB::raw('YEAR(tanggal)'))->pluck('tahun');
        
        $data = [
            'rservers' => $rserver,
            'tahun' => $tahun,
            'status' => Status::all()
        ];
        return view('ruangserver.ruangserver', $data);
    }

    public function add()
    {
        return view('ruangserver.add-ruangserver');
    }

    public function create(Request $request)
    {     
        try{
            DB::beginTransaction();
            $validator = Validator::make($request->all(), [
                'nama' => 'required',
                'perusahaan' => 'required',
                'jabatan' => 'required',
                'no_ktp' => 'required',
                'tanggal' => 'required',
                'waktu_datang' => 'required',
                'waktu_meninggalkan' => 'required',
                'izin_foto' => 'required',
            ]);
    
            if ($validator->fails()) {
                return redirect('/ruangserver/add/')->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
            }
            $aktivitas = implode(',', $request->aktivitas);

            $ruang_server = Ruangserver::create([
                'no_ktp' => $request->no_ktp,
                'tanggal' => $request->tanggal,
                'waktu_datang' => $request->waktu_datang,
                'waktu_meninggalkan' => $request->waktu_meninggalkan,
                'izin_foto' => $request->izin_foto,
                'catatan' => $request->catatan,
                'aktivitas' => $aktivitas,
                'user_id' => $request->user_id,
                'status' => $request->status,
            ]);
    
            if (count($request->nama)>0) {
                foreach ($request->nama as $item => $value) {
                    $data2 = array(
                        'ruang_server_id' => $ruang_server->id,
                        'nama' => $request['nama'][$item],
                        'perusahaan' => $request['perusahaan'][$item],
                        'jabatan' => $request['jabatan'][$item],
                    );
                    Pemohon::create($data2);
                }
            }

            DB::commit();
                return redirect('ruangserver')->with('successServer', 'Ruang Server berhasil ditambahkan!');
            }catch(\Exception $e){
                DB::rollback(); 
                return redirect()->back()->with('warning','Something Went Wrong!');
            }   
    }

    public function edit($id)
    {
        return view('ruangserver.edit-ruangserver', [
            'rservers' => Ruangserver::with('tim_pemohon')->find($id),
        ]);
    }

    public function proses($id)
    {
        $nomor_kartu = Ruangserver::generateNoKartu();
        return view('ruangserver.proses-ruangserver', [
            'rservers' => Ruangserver::with('user', 'statusserver', 'timpemohon')->find($id),
            'nomor_kartu' => $nomor_kartu
        ]);
    }

    public function detail($id)
    {
        return view('ruangserver.detail-ruangserver', [
            'rservers' => Ruangserver::with('user', 'statusserver', 'timpemohon')->find($id),
        ]);
    }

    public function prosesUpdate(Request $request, $id)
    {
        $server = Ruangserver::find($id);
        
        $rules = [
            'no_rak' => 'required',
            'no_kartu_akses' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        $no_kartu = Ruangserver::generateNoKartu();
        $request->merge(['no_kartu_akses' => $no_kartu]);

        $server->update([
            'no_rak' => $request->no_rak,
            'no_kartu_akses' => $request->no_kartu_akses,
            'status'=> $request->status
        ]);

        return redirect('/ruangserver')->with('success', 'Status berhasil diubah!');
    }

    public function uploadFormulir(Request $request, $id)
    {   
        $rserver = Ruangserver::find($id);

        $validator = Validator::make($request->all(), [
            'surat' => 'required|file|mimes:pdf|max:5120'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->file('surat')) {
            $nama_file = date('Ymdhis') . '_' . $request->file('surat')->getClientOriginalName();
            $request->file('surat')->move('form_server/', $nama_file);
            if ($request->oldFile) {
                unlink('form_server/'. $request->oldFile);
            }
        } else {
            $nama_file = $request->oldFile;
        }

        $rserver->update([
            'surat' => $nama_file,
        ]);

        return redirect('/ruangserver')->with('success', 'Formulir permohonan berhasil diupload!');
    }

    public function status(Request $request, $id)
    {
        $rserver = Ruangserver::find($id);

        $rserver->update([
            'status' => $request->status,
            'keterangan' => $request->keterangan,
        ]);

        return redirect('/ruangserver')->with('success', 'Status berhasil diubah!');
    }

    public function delete($id)
    {
        $ruang = Ruangserver::find($id);
        Pemohon::where('ruang_server_id', $ruang->id)->delete();
        $ruang->delete();
        return redirect('/ruangserver')->with('success', 'Permohonan izin akses ruang server berhasil dihapus!');
    }

    public function cetak($id)
    {
        $filename = "ruangserver.pdf";
        $data = [
            'title' => 'Formulir Permohonan Email',
            'rs' => Ruangserver::find($id),
            'kominfo' => User::where('role_id', 1)->find(1)
        ];
        $html = view('ruangserver.cetak', $data);

        // dd($data['kominfo']);

        $pdf = new TCPDF;

        $pdf::SetTitle('RuangServer');
        $pdf::SetHeaderMargin(30); // set margin untuk header
        $pdf::AddPage('P', 'F4');
        $pdf::Image('img/kabsukoharjo.jpeg', 10, 10, 25);
        $pdf::SetFont('times', 'B', 14);
        $pdf::Cell(0, 10, 'DINAS KOMUNIKASI DAN INFORMATIKA', 0, 1, 'C');
        $pdf::Cell(0, 10, 'KABUPATEN SUKOHARJO', 0, 1, 'C');
        $pdf::SetFont('times', '', 12);
        $pdf::Cell(0, 10, $data['kominfo']['opd']['alamat'], 0, 1, 'C');
        $pdf::Cell(0, 10, 'Telp. '.$data['kominfo']['opd']['telp'].', email: '. $data['kominfo']['opd']['email'], 0, 1, 'C');
        $pdf::SetLineWidth(0.1); // Membuat garis bawah pada header
        $pdf::Line(10, 50, 200, 50);
        $pdf::SetMargins(15, 25, 25);
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output($filename, 'I');
    }

}
