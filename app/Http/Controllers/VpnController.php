<?php

namespace App\Http\Controllers;

use App\Models\Vpn;
use App\Models\Status;
use Illuminate\Http\Request;
use Elibyy\TCPDF\Facades\TCPDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class VpnController extends Controller
{
    public function index(Request $request)
    {
        if ($request->tahun) {
            if (Auth::user()->role_id == 2) {
                $vpn = Vpn::where('user_id', Auth::user()->id)->where(DB::raw('YEAR(created_at)'), $request->tahun)->latest()->get();
            } else {
                $vpn = Vpn::where(DB::raw('YEAR(created_at)'), $request->tahun)->latest()->get();
            }
        } else {
            if (Auth::user()->role_id == 2) {
                $vpn = Vpn::where('user_id', Auth::user()->id)->where(DB::raw('YEAR(created_at)'), now())->latest()->get();
            } else {
                $vpn = Vpn::where(DB::raw('YEAR(created_at)'), now())->latest()->get();
            }
        }
        
        $tahun = DB::table('vpn')->select(DB::raw('YEAR(created_at) as tahun'))->orderBy(DB::raw('YEAR(created_at)'), 'desc')->groupBy(DB::raw('YEAR(created_at)'))->pluck('tahun');
        
        $data = [
            'vpns' => $vpn,
            'tahun' => $tahun,
            'jenispermohon' => Vpn::select('jenis_permohonan')->groupBy('jenis_permohonan')->pluck('jenis_permohonan'),
            'status' => Status::all()
        ];
        return view('vpn.vpn', $data);
    }

    public function baru()
    {
        $no_vpn = Vpn::generateNomor();
        return view('vpn.baru-vpn', [
            'no_vpn' => $no_vpn
        ]);
    }

    public function createBaru(Request $request)
    {        
        $validator = Validator::make($request->all(), [
            'keperluan' => 'required',
            'tgl_awal' => 'required|date',
            'tgl_awal' => 'required|date',
            'password' => 'required|string|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%@&]).*$/',
        ]);

        if ($validator->fails()) {
            return redirect('/vpn/baru')->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        
        Vpn::create([
            'user_id' => $request->user_id,
            'jenis_permohonan' => $request->jenis_permohonan,
            'keperluan' => $request->keperluan,
            'tgl_awal' => $request->tgl_awal,
            'tgl_berakhir' => $request->tgl_berakhir,
            'password' => $request->password,
            'status' => $request->status,
            'no_vpn' => $request->no_vpn,
        ]);

        return redirect('/vpn')->with('successPermohonan', 'Permohonan baru VPN berhasil dibuat!');
    }

    public function edit($id)
    {
        return view('vpn.edit-vpn', [
            'vpn' => Vpn::find($id)
        ]);
    }

    public function update(Request $request, $id)
    {
        $vpn = Vpn::find($id);

        if ($vpn->jenis_permohonan == 'Baru') {
            $rules = [
                'keperluan' => 'required',
                'tgl_awal' => 'required|date',
                'tgl_awal' => 'required|date',
                'password' => 'required|string|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%@&]).*$/',
            ];
        } elseif ($vpn->jenis_permohonan == 'Ganti Password') {
            $rules = [
                'permasalahan' => 'required',
                'ip_vpn' => 'required|ip',
                'password' => 'required|string|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%@&]).*$/',
            ];
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/vpn/edit/'. $vpn->id)->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($vpn->jenis_permohonan == 'Baru') {
            $vpn->update([
                'keperluan' => $request->keperluan,
                'tgl_awal' => $request->tgl_awal,
                'tgl_berakhir' => $request->tgl_berakhir,
                'password' => $request->password,
            ]);
        } elseif ($vpn->jenis_permohonan == 'Ganti Password') {
            $vpn->update([
                'permasalahan' => $request->permasalahan,
                'ip_vpn' => $request->ip_vpn,
                'password' => $request->password,
            ]);
        }

        return redirect('/vpn')->with('success', 'Permohonan VPN berhasil diubah!');
    }

    public function gantipass($id)
    {
        $no_vpn = Vpn::generateNomor();
        return view('vpn.gantipass-vpn', [
            'vpn' => Vpn::find($id),
            'no_vpn' => $no_vpn
        ]);
    }

    public function createGantipass(Request $request, $id)
    {
        $vpn = Vpn::find($id);

        $rules = [
            'permasalahan' => 'required',
            'ip_vpn' => 'required|ip',
            'password' => 'required|string|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%@&]).*$/',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('/vpn/gantipass/'. $vpn->id)->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }
        
        $vpn->update([
            'is_changed' => 1,
        ]);

        Vpn::create([
            'user_id' => $request->user_id,
            'jenis_permohonan' => $request->jenis_permohonan,
            'permasalahan' => $request->permasalahan,
            'ip_vpn' => $request->ip_vpn,
            'password' => $request->password,
            'status' => $request->status,
            'no_vpn' => $request->no_vpn,
            'keperluan' => $vpn->keperluan,
            'tgl_awal' => $vpn->tgl_awal,
            'tgl_berakhir' => $vpn->tgl_berakhir,
            'old_vpn_id' => $vpn->id,
        ]);

        return redirect('/vpn')->with('successPermohonan', 'Permohonan ganti password VPN berhasil dibuat!');
    }

    public function uploadFormulir(Request $request, $id)
    {   
        $vpn = Vpn::find($id);

        $validator = Validator::make($request->all(), [
            'surat' => 'required|file|mimes:pdf|max:5120'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', $validator->messages()->all()[0])->withErrors($validator)->withInput();
        }

        if ($request->file('surat')) {
            $nama_file = date('Ymdhis') . '_' . $request->file('surat')->getClientOriginalName();
            $request->file('surat')->move('form_vpn/', $nama_file);
            if ($request->oldFile) {
                unlink('form_vpn/'. $request->oldFile);
            }
        } else {
            $nama_file = $request->oldFile;
        }

        $vpn->update([
            'surat' => $nama_file,
        ]);

        return redirect('/vpn')->with('success', 'Formulir permohonan berhasil diupload!');
    }

    public function status(Request $request, $id)
    {
        $vpn = Vpn::find($id);

        if ($request->status == 4) {
            Vpn::where('id', $vpn->old_vpn_id)->update([
                'is_changed' => null
            ]);
        }
        $vpn->update([
            'status' => $request->status,
            'keterangan' => $request->keterangan,
        ]);

        return redirect('/vpn')->with('success', 'Status berhasil diubah!');
    }

    public function delete($id)
    {
        $delete = Vpn::find($id);
        $delete->delete();
        return redirect('/vpn')->with('success', 'Permohonan VPN berhasil dihapus!');
    }

    public function cetak($id)
    {
        $filename = "vpn.pdf";
        $data = [
            'title' => 'Formulir Permohonan VPN',
            'vpn' => Vpn::find($id),
            'jenispermohon' => Vpn::select('jenis_permohonan')->groupBy('jenis_permohonan')->pluck('jenis_permohonan')
        ];
        $html = view('vpn.cetak', $data);

        $pdf = new TCPDF;

        $pdf::SetTitle('VPN');
        $pdf::SetHeaderMargin(30); // set margin untuk header
        $pdf::AddPage('P', 'F4');
        $pdf::Image('img/kabsukoharjo.jpeg', 10, 10, 25);
        $pdf::SetFont('times', 'B', 14);
        $pdf::Cell(0, 10, $data['vpn']['user']['opd']['nama']. ' Kabupaten Sukoharjo', 0, 1, 'C');
        $pdf::SetFont('times', '', 12);
        $pdf::Cell(0, 10, $data['vpn']['user']['opd']['alamat'], 0, 1, 'C');
        $pdf::Cell(0, 10, 'Telp. '.$data['vpn']['user']['opd']['telp'].', email: '. $data['vpn']['user']['opd']['email'], 0, 1, 'C');
        $pdf::Ln(10); // jarak antara header dan konten
        $pdf::SetLineWidth(0.1); // Membuat garis bawah pada header
        $pdf::Line(10, 45, 200, 45);
        $pdf::SetMargins(15, 25, 25);
        $pdf::writeHTML($html, true, false, true, false, '');
        $pdf::Output($filename, 'I');
    }
}
