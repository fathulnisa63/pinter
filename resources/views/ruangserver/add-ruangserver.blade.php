@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="/ruangserver" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Permohonan Izin Akses Ruang Server</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="/ruangserver">Ruang Server</a></div>
                <div class="breadcrumb-item">Permohonan Izin Akses Ruang Server</div>
            </div>
        </div>

        <h2 class="section-title">Form Permohonan Izin Akses Ruang Server</h2>
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <form action="{{ url('/ruangserver/store') }}" method="post">
                        @csrf
                        <div class="card card-primary">
                            <div class="card-header">
                                <h4>Informasi Pemohon</h4>
                            </div>
                            <div class="card-body">
                                <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">
                                <input type="hidden" name="status" value="1">
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Nama</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control" name="nama[]"
                                            value="{{ old('nama') }}" id="nama" required>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Perusahaan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control" name="perusahaan[]"
                                            value="{{ old('perusahaan') }}" id="perusahaan" required>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Jabatan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control" name="jabatan[]"
                                            value="{{ old('jabatan') }}" id="jabatan" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for=""></label>
                                    <a href="#" class="addpemohon btn btn-primary" style="float: right;">Tambah
                                        Pemohon</a>
                                </div>
                                <div class="pemohon"></div>
                                <hr>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Nomor KTP
                                        PIC</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="number" class="form-control @error('no_ktp') is-invalid @enderror"
                                            name="no_ktp" value="{{ old('no_ktp') }}" id="no_ktp" required>
                                        @error('no_ktp')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-danger">
                            <div class="card-header">
                                <h4>Rincian Permohonan</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Tanggal</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="date" class="form-control @error('tanggal') is-invalid @enderror"
                                            name="tanggal" value="{{ old('tanggal') }}" id="tanggal" required>
                                        @error('tanggal')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Waktu
                                        Datang</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="time"
                                            class="form-control @error('waktu_datang') is-invalid @enderror"
                                            name="waktu_datang" value="{{ old('waktu_datang') }}" id="waktu_datang"
                                            required>
                                        @error('waktu_datang')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Waktu
                                        Meninggalkan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="time"
                                            class="form-control @error('waktu_meninggalkan') is-invalid @enderror"
                                            name="waktu_meninggalkan" value="{{ old('waktu_meninggalkan') }}"
                                            id="waktu_meninggalkan" required>
                                        @error('waktu_meninggalkan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 d-block required">Aktivitas</label>
                                    <div class="form-group">
                                        <div class="form-check">
                                            <input class="form-check-input @error('aktivitas') is-invalid @enderror"
                                                name="aktivitas[]" value="Pemasangan" type="checkbox"
                                                id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Pemasangan
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input @error('aktivitas') is-invalid @enderror"
                                                name="aktivitas[]" value="Perawatan" type="checkbox" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Perawatan
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input @error('aktivitas') is-invalid @enderror"
                                                name="aktivitas[]" value="Pembongkaran" type="checkbox"
                                                id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Pembongkaran
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input @error('aktivitas') is-invalid @enderror"
                                                name="aktivitas[]" value="Perbaikan" type="checkbox" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Perbaikan
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input @error('aktivitas') is-invalid @enderror"
                                                name="aktivitas[]" value="Setup" type="checkbox" id="defaultCheck1">
                                            <label class="form-check-label" for="defaultCheck1">
                                                Setup
                                            </label>
                                        </div>
                                    </div>

                                    @error('aktivitas')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 d-block required">Izin
                                        Pengambilan Gambar</label>
                                    <div class="form-group">
                                        <div class="form-check">
                                            <input class="form-check-input @error('izin_foto') is-invalid @enderror"
                                                type="radio" name="izin_foto" value="Ya" id="exampleRadios1"
                                                checked="">
                                            <label class="form-check-label" for="exampleRadios1">
                                                Ya
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input @error('izin_foto') is-invalid @enderror"
                                                type="radio" name="izin_foto" value="Tidak" id="exampleRadios2"
                                                checked="">
                                            <label class="form-check-label" for="exampleRadios2">
                                                Tidak
                                            </label>
                                        </div>
                                    </div>
                                    @error('izin_foto')
                                        <div class="invalid-feedback d-block">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Catatan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea class="form-control @error('catatan') is-invalid @enderror" style="height: auto" name="catatan" id="catatan">{{ old('catatan') }}</textarea>
                                        @error('catatan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-primary" id="btn-simpan"
                                            disabled>Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script>
        const checkboxes = document.querySelectorAll('input[name="aktivitas[]"]');
        const submitButton = document.querySelector('button[type="submit"]');

        checkboxes.forEach((checkbox) => {
            checkbox.addEventListener('change', () => {
                if (document.querySelector('input[name="aktivitas[]"]:checked')) {
                    submitButton.disabled = false;
                } else {
                    submitButton.disabled = true;
                }
            });
        });
    </script>
    <script>
        const input = document.querySelector('#no_ktp');
        input.addEventListener('input', () => {
          const value = input.value;
          if (value.length < 16 || value.length > 16) {
            input.setCustomValidity('Nomor KTP harus 16 digit.');
          } else {
            input.setCustomValidity('');
          }
        });
      </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script>
        var button_add = document.getElementById('addpemohon');
        $('.addpemohon').on('click', function() {
            addpemohon();
        });

        function addpemohon() {
            var pemohon =
                '<div><div class="form-group row mb-4"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Nama</label><div class="col-sm-12 col-md-7"><input type="text" class="form-control" name="nama[]" value="{{ old('nama') }}" id="nama" required></div></div><div class="form-group row mb-4"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Perusahaan</label><div class="col-sm-12 col-md-7"><input type="text"class="form-control" name="perusahaan[]" value="{{ old('perusahaan') }}" id="perusahaan" required></div></div><div class="form-group row mb-4"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Jabatan</label><div class="col-sm-12 col-md-7"><input type="text" class="form-control" name="jabatan[]" value="{{ old('jabatan') }}" id="jabatan" required></div></div><label for=""></label><a href="#" class="remove btn btn-danger" style="float: right;">Hapus</a></div></div>';
            $('.pemohon').append(pemohon);
        };

        $('.remove').live('click', function() {
            $(this).parent().remove();
        })
    </script>
@endsection
