@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="/hosting" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Permohonan Baru Hosting</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="/hosting">Hosting</a></div>
                <div class="breadcrumb-item">Permohonan Baru Hosting</div>
            </div>
        </div>

        <h2 class="section-title">Form Permohonan Baru Hosting</h2>
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <form action="{{ url('/hosting/store') }}" method="post">
                        @csrf
                        <div class="card card-primary">
                            <div class="card-header">
                                <h4>Instansi</h4>
                            </div>
                            <div class="card-body">
                                <input type="hidden" class="form-control" name="jenis_permohonan" value="Baru">
                                <input type="hidden" class="form-control" name="user" value="{{ Auth::user()->id }}">
                                <input type="hidden" class="form-control" name="no_hosting" value="{{ $no_hosting }}">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Nama
                                        Kepala Instansi</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text"
                                            class="form-control @error('nama_kepala') is-invalid @enderror"
                                            name="nama_kepala" value="{{ old('nama_kepala') }}" id="nama_kepala" required>
                                        @error('nama_kepala')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">NIP
                                        Kepala</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="number" class="form-control @error('nip_kepala') is-invalid @enderror"
                                            name="nip_kepala" value="{{ old('nip_kepala') }}" id="nip_kepala" required>
                                        @error('nip_kepala')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-danger">
                            <div class="card-header">
                                <h4>Admin OPD</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Nama</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                            name="nama" value="{{ old('nama') }}" id="nama" required>
                                        @error('nama')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">NIP</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('nip') is-invalid @enderror"
                                            name="nip" value="{{ old('nip') }}" id="nip">
                                        @error('nip')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Jabatan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('jabatan') is-invalid @enderror"
                                            name="jabatan" value="{{ old('jabatan') }}" id="jabatan" required>
                                        @error('jabatan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Email</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                                            name="email" value="{{ old('email') }}" id="email" required>
                                        @error('email')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Telepon</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="number" class="form-control @error('phone') is-invalid @enderror"
                                            name="phone" value="{{ old('phone') }}" id="phone" required>
                                        @error('phone')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card card-info">
                            <div class="card-header">
                                <h4>Deskripsi Website</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Deskripsi
                                        Website</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea class="form-control @error('deskripsi_web') is-invalid @enderror" style="height: auto" name="deskripsi_web" id="deskripsi_web"
                                            required>{{ old('deskripsi_web') }}</textarea>
                                        @error('deskripsi_web')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Jenis
                                        Hosting</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="custom-select form-control @error('jenis_hosting') is-invalid @enderror"
                                            data-width="100%" name="jenis_hosting" id="selectOption" required>
                                            <option selected>-- Silahkan Pilih --</option>
                                            <option value="VPS">VPS</option>
                                            <option value="Cpanel">Cpanel</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="inputBox" style="display: none;">
                                    <div class="form-group row mb-4">
                                        <label
                                            class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Operating
                                            System</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control @error('os') is-invalid @enderror"
                                                name="os" value="{{ old('os') }}" id="os" required>
                                            @error('os')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label
                                            class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Processor</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text"
                                                class="form-control @error('processor') is-invalid @enderror"
                                                name="processor" value="{{ old('processor') }}" id="processor" required>
                                            @error('processor')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label
                                            class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">RAM</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control @error('ram') is-invalid @enderror"
                                                name="ram" value="{{ old('ram') }}" id="ram" required>
                                            @error('ram')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Storage</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('storage') is-invalid @enderror"
                                            name="storage" value="{{ old('storage') }}" id="storage" required>
                                        @error('storage')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Nama
                                        Subdomain</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text"
                                            class="form-control @error('subdomain_baru') is-invalid @enderror"
                                            name="subdomain_baru" value="{{ old('subdomain_baru') }}"
                                            id="subdomain_baru" required>
                                        @error('subdomain_baru')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <input type="hidden" name="status" value="1">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-primary" id="btn-simpan">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <script>
        document.getElementById('selectOption').addEventListener('change', function() {
            var selectedOption = this.options[this.selectedIndex].value;
            var inputBox = document.getElementById('inputBox');
            var os = document.getElementById('os');
            var processor = document.getElementById('processor');
            var ram = document.getElementById('ram');

            if (selectedOption === 'VPS') {
                inputBox.style.display = 'block';
                os.setAttribute('required', '');
                processor.setAttribute('required', '');
                ram.setAttribute('required', '');
            } else {
                inputBox.style.display = 'none';
                os.removeAttribute('required');
                processor.removeAttribute('required');
                ram.removeAttribute('required');
            }
        });
    </script>
@endsection
