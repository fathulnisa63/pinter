@extends('layouts.home')

@section('content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="/hakakses" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Permintaan Hak Akses</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="/dashboard">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="/hakakses">Hak Akses</a></div>
                <div class="breadcrumb-item">Permintaan Hak Akses</div>
            </div>
        </div>

        <h2 class="section-title">Form Permintaan Hak Akses</h2>
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <form action="{{ url('hakakses/create') }}" method="post">
                        @csrf
                        <input type="hidden" class="form-control" name="no_hak_akses" value="{{ $no_hak_akses }}">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h4>Penanggungjawab Pihak Ketiga</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Nama
                                        Perusahaan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text"
                                            class="form-control @error('nama_perusahaan') is-invalid @enderror"
                                            name="nama_perusahaan" value="{{ old('nama_perusahaan') }}" id="nama_perusahaan"
                                            required>
                                        @error('nama_perusahaan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Nama</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                            name="nama" value="{{ old('nama') }}" id="nama" required>
                                        @error('nama')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Telepon</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('telp') is-invalid @enderror"
                                            name="telp" value="{{ old('telp') }}" id="telp" required>
                                        @error('telp')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Jabatan di
                                        Perusahaan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('jabatan') is-invalid @enderror"
                                            name="jabatan" value="{{ old('jabatan') }}" id="jabatan" required>
                                        @error('jabatan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card card-info">
                            <div class="card-header">
                                <h4>Detail Hak Akses</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Tujuan/Jenis
                                        Akses</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select id="jenisakses" class="selectpicker" data-width="100%" data-live-search="true" name="jenis_hak_akses" required>
                                            <option value="">Pilih Jenis Akses</option>
                                            @foreach ($jenisakses as $jen)
                                                <option value="{{ $jen->id }}" {{ old('jenis_hak_akses') == $jen->id ? 'selected' : '' }}>{{ $jen->nama }}</option>
                                            @endforeach
                                            <option value="lainnya">Lainnya</option>
                                        </select>
                                        <div id="inputLainnya" style="display:none;">
                                            <input type="text" class="form-control mt-2" id="inputLainnyaText"
                                                name="jenisakses_lain" placeholder="Tulis tujuan/jenis akses lainnya disini.." autofocus>
                                                @error('jenisakses_lain')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Alasan Permintaan</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea class="form-control @error('alasan') is-invalid @enderror" style="height: auto" name="alasan" id="alasan" required>{{ old('alasan') }}</textarea>
                                        @error('alasan')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Waktu</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" class="form-control @error('waktu') is-invalid @enderror"
                                            name="waktu" value="{{ old('waktu') }}" id="waktu" required placeholder="Contoh: 2 bulan">
                                        @error('waktu')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3 required">Masa Berlaku</label>
                                    <div class="col-sm-12 col-md-3">
                                        <input type="text"
                                            class="form-control datepicker @error('tanggal_awal') is-invalid @enderror"
                                            name="tanggal_awal" value="{{ old('tanggal_awal') }}" id="tanggal_awal"
                                            required>
                                        @error('tanggal_awal')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="col-sm-12 col-md-1 d-flex justify-content-center align-items-center"><i class="fas fa-minus"></i></div>
                                    <div class="col-sm-12 col-md-3">
                                        <input type="text"
                                            class="form-control datepicker @error('tanggal_akhir') is-invalid @enderror"
                                            name="tanggal_akhir" value="{{ old('tanggal_akhir') }}" id="tanggal_akhir"
                                            required>
                                        @error('tanggal_akhir')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                <input type="hidden" name="status" value="1">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-primary" id="btn-simpan">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
