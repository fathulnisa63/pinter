<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Hak Akses</title>
</head>

<body>
    <div>
        <table border="0.5" cellpadding="5" width="100%">
            <tbody>
                <tr>
                    <th align="center" width="20%" rowspan="2"><img src="{{ public_path('img/kabsukoharjo.jpeg') }}" height="100px" alt=""></th>
                    <th align="center" width="40%" rowspan="2"><div style="vertical-align: middle"><h3>FORMULIR PERMINTAAN HAK AKSES</h3></div></th>
                    <th align="left" width="40%" height="50px">Nomor : <div style="text-align: center">{{ $hakakses->no_hak_akses }}</div></th>
                </tr>
                <tr>
                    <td>Tanggal : <div style="text-align: center">{{ date('d F Y', strtotime($hakakses->tanggal_awal)) }}</div></td>
                </tr>
                <tr>
                    <th colspan="2" width="50%" align="center"><h5>PENANGGUNGJAWAB OPD</h5></th>
                    <th colspan="2" width="50%" align="center"><h5>PENANGGUNGJAWAB PIHAK KE-III</h5></th>
                </tr>
                <tr>
                    <td colspan="2" width="50%">Nama OPD : {{ $hakakses->user->opd->nama }}</td>
                    <td colspan="2" width="50%">Nama Perusahaan : {{ $hakakses->pihakketiga->nama_perusahaan }}</td>
                </tr>
                <tr>
                    <td colspan="2" width="50%">Nama : {{ $hakakses->user->nama }}</td>
                    <td colspan="2" width="50%">Nama : {{ $hakakses->pihakketiga->nama }}</td>
                </tr>
                <tr>
                    <td colspan="2" width="50%">NIP : {{ $hakakses->user->nip }}</td>
                    <td colspan="2" width="50%">Telp : {{ $hakakses->pihakketiga->telp }}</td>
                </tr>
                <tr>
                    <td colspan="2" width="50%">Telp : {{ $hakakses->user->opd->telp }}</td>
                    <td colspan="2" width="50%">Jabatan di Perusahaan : {{ $hakakses->pihakketiga->jabatan }}</td>
                </tr>
                <tr>
                    <td colspan="3" width="100%">Tujuan/Jenis Akses : {{ $hakakses->jenisakses->nama }}</td>
                </tr>
                <tr>
                    <td colspan="3" width="100%">Alasan Permintaan Hak Akses : <br><br> {{ $hakakses->alasan }}</td>
                </tr>
                <tr>
                    <td colspan="2" width="50%">Masa Berlaku Hak Akses :</td>
                    <td colspan="2" width="50%">Waktu : {{ $hakakses->waktu }}</td>
                </tr>
                <tr>
                    <td colspan="2" width="50%">Tanggal Awal : {{ date('d M Y', strtotime($hakakses->tanggal_awal)) }}</td>
                    <td colspan="2" width="50%"></td>
                </tr>
                <tr>
                    <td colspan="2" width="50%">Tanggal Awal : {{ date('d M Y', strtotime($hakakses->tanggal_akhir)) }}</td>
                    <td colspan="2" width="50%"></td>
                </tr>
                <tr>
                    <td colspan="3" width="100%"><b>Ketentuan Penggunaan Hak Akses : </b>
                        <ul>
                            <li>User harus menyetujui dan mematuhi kebijakan keamanan informasi Dinas Komunikasi dan Informatika Kab. Sukoharjo dan prosedur pengamanan terkait lainnya.</li>
                            <li>User dilarang mengalihkan dan/atau meminjamkan hak akses kepada pihak lain.</li>
                            <li>User dilarang menyalahgunakan akses untuk kepentingan selain penugasan.</li>
                        </ul> <br>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" width="50%" align="center"><b>PENANGGUNGJAWAB OPD<br><br><br><br><br><br><br>{{ $hakakses->user->nama }} <br> NIP.{{ $hakakses->user->nip }}</b></td>
                    <td colspan="2" width="50%" align="center"><b>PENANGGUNGJAWAB PIHAK KE-III<br><br><br><br><br><br><br>{{ $hakakses->pihakketiga->nama }} <br> {{ $hakakses->pihakketiga->jabatan }}</b></td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>
