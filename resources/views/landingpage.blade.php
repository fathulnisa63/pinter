<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PINTER | Pelayanan Admnistrasi Sistem Elektronik</title>
    <link rel="stylesheet" href="{{ asset('assets/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/owl.carousel/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/owl.carousel/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/aos/css/aos.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/jquery-flipster/css/jquery.flipster.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="shortcut icon" href="{{ asset('assets/images/logo/rectangle.png') }}" />
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">
    <div id="mobile-menu-overlay"></div>
    <nav class="navbar navbar-expand-lg fixed-top">
        <div class="container">
            <a class="navbar-brand" href="#"><img style="width: 20%"
                    src="{{ asset('assets/images/logo/horizontal.png') }}" alt="Pinter"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
                aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><i class="mdi mdi-menu"> </i></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                <div class="d-lg-none d-flex justify-content-between px-4 py-3 align-items-center">
                    <img src="{{ asset('assets/images/logo-dark.svg') }}" class="logo-mobile-menu" alt="logo">
                    <a href="javascript:;" class="close-menu"><i class="mdi mdi-close"></i></a>
                </div>
                <ul class="navbar-nav ml-auto align-items-center">
                    <li class="nav-item active">
                        <a class="nav-link" href="#home">Beranda <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#services">Layanan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#about">Tentang</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Bantuan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-success" href="/login">Masuk</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="page-body-wrapper">
        <section id="home" class="home">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="main-banner">
                            <div class="d-sm-flex justify-content-between">
                                <div data-aos="zoom-in-up">
                                    <div class="banner-title">
                                        <h3 class="font-weight-medium"><span style="color: yellow">P</span>elayanan
                                            Adm<span style="color: yellow">in</span>istrasi Sis<span
                                                style="color: yellow">te</span>m Elekt<span
                                                style="color: yellow">r</span>onik
                                        </h3>
                                    </div>
                                    <p class="mt-3">Dinas Komunikasi dan Informatika Kabupaten Sukoharjo</p>
                                    <a href="#services" style="width: 30%" class="nav-link btn btn-secondary mt-3">Apa
                                        Saja
                                        Layananya?</a>
                                </div>
                                <div class="mt-5 mt-lg-0">
                                    <img src="{{ asset('assets/images/ilustration.png') }}" alt="pinter"
                                        class="img-fluid" data-aos="zoom-in-up">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="our-services" id="services">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        {{-- <h5 class="text-dark">Layanan</h5> --}}
                        <h3 class="font-weight-medium text-dark mb-5">Layanan</h3>
                    </div>
                </div>
                {{-- ROW 1 --}}
                <div class="row" data-aos="fade-up">
                    <div class="col-sm-4 text-center text-lg-left">
                        <div class="services-box" data-aos="fade-down" data-aos-easing="linear"
                            data-aos-duration="1500">
                            <img src="{{ asset('assets/images/services/recomendation.png') }}" style="width: 20%"
                                alt="rekomendasi" data-aos="zoom-in">
                            <h6 class="text-dark mb-3 mt-4 font-weight-medium">Rekomendasi</h6>
                            <p>Persetujuan terhadap pengajuan rencana investasi TIK OPD di Pemda Sukoharjo
                            </p>
                            <a href="/login" class="btn btn-secondary mt-3">Akan Datang</a>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center text-lg-left">
                        <div class="services-box" data-aos="fade-down" data-aos-easing="linear"
                            data-aos-duration="1500">
                            <img src="{{ asset('assets/images/services/subdomain.png') }}" style="width: 20%"
                                alt="subdomain" data-aos="zoom-in">
                            <h6 class="text-dark mb-3 mt-4 font-weight-medium">Subdomain</h6>
                            <p>Layanan subdomain *.sukoharjokab.go.id bagi OPD dan
                                stakeholder Pemda Sukoharjo</p>
                            <a href="/login" class="btn btn-secondary mt-3">Akan Datang</a>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center text-lg-left">
                        <div class="services-box" data-aos="fade-down" data-aos-easing="linear"
                            data-aos-duration="1500">
                            <img src="{{ asset('assets/images/services/hosting.png') }}" style="width: 20%"
                                alt="email" data-aos="zoom-in">
                            <h6 class="text-dark mb-3 mt-4 font-weight-medium">Hosting</h6>
                            <p>Layanan hosting untuk aplikasi yang dikelola OPD atau stakeholder Pemda Sukoharjo</p>
                            <a href="/login" class="btn btn-secondary mt-3">Akan Datang</a>
                        </div>
                    </div>
                </div>
                {{-- ROW 2 --}}
                <div class="row" data-aos="fade-up">
                    <div class="col-sm-4 text-center text-lg-left">
                        <div class="services-box" data-aos="fade-down" data-aos-easing="linear"
                            data-aos-duration="1500">
                            <img src="{{ asset('assets/images/services/penetration.png') }}" style="width: 20%"
                                alt="rekomendasi" data-aos="zoom-in">
                            <h6 class="text-dark mb-3 mt-4 font-weight-medium">Pentest</h6>
                            <p>Pengujian tingkat keamanan suatu Aplikasi sebelum di publish atau digunakan
                            </p>
                            <a href="/login" class="btn btn-secondary mt-3">Akan Datang</a>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center text-lg-left">
                        <div class="services-box" data-aos="fade-down" data-aos-easing="linear"
                            data-aos-duration="1500">
                            <img src="{{ asset('assets/images/services/email.png') }}" style="width: 20%"
                                alt="email" data-aos="zoom-in">
                            <h6 class="text-dark mb-3 mt-4 font-weight-medium">Email</h6>
                            <p>Layanan email meliputi pembuatan email baru, reset password, tambah kuota email</p>
                            <a href="/login" class="btn btn-secondary mt-3">Akan Datang</a>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center text-lg-left">
                        <div class="services-box" data-aos="fade-down" data-aos-easing="linear"
                            data-aos-duration="1500">
                            <img src="{{ asset('assets/images/services/vpn.png') }}" style="width: 20%"
                                alt="vpn" data-aos="zoom-in">
                            <h6 class="text-dark mb-3 mt-4 font-weight-medium">Virtual Private Network</h6>
                            <p>Layanan teknologi jaringan komunikasi untuk dapat mengakses pada jaringan intra </p>
                            <a href="/login" class="btn btn-secondary mt-3">Akan Datang</a>
                        </div>
                    </div>
                </div>
                {{-- ROW 3 --}}
                <div class="row" data-aos="fade-up">
                    <div class="col-sm-4 text-center text-lg-left">
                        <div class="services-box pb-0" data-aos="fade-down" data-aos-easing="linear"
                            data-aos-duration="1500">
                            <img src="{{ asset('assets/images/services/suko-drive.png') }}" alt="suko-drive"
                                style="width: 20%" data-aos="zoom-in">
                            <h6 class="text-dark mb-3 mt-4 font-weight-medium">Suko Drive</h6>
                            <p>Layanan penggunaan penyimpanan berbasis cloud
                                untuk OPD dan stakeholder</p>
                            <a href="/login" class="btn btn-secondary mt-3">Akan Datang</a>
                        </div>
                    </div>
                    <div class="col-sm-4 text-center text-lg-left">
                        <div class="services-box pb-lg-0" data-aos="fade-down" data-aos-easing="linear"
                            data-aos-duration="1500">
                            <img src="{{ asset('assets/images/services/tte.png') }}" style="width: 20%"
                                alt="tte" data-aos="zoom-in">
                            <h6 class="text-dark mb-3 mt-4 font-weight-medium">Tanda Tangan Elektronik</h6>
                            <p>Layanan penggunaan sertifikat elektronik yang diterbitkan oleh
                                Balai Sertifikasi Elektronik (BSrE)</p>
                            <a href="/login" class="btn btn-secondary mt-3">Akan Datang</a>
                        </div>
                    </div>
                    {{-- <div class="col-sm-4 text-center text-lg-left">
                        <div class="services-box" data-aos="fade-down" data-aos-easing="linear"
                            data-aos-duration="1500">
                            <img src="{{ asset('assets/images/services/complaint.png') }}" style="width: 20%"
                                alt="vpn" data-aos="zoom-in">
                            <h6 class="text-dark mb-3 mt-4 font-weight-medium">Pengaduan Masyarakat</h6>
                            <p>Layanan pengaduan masyarakat/ desa untuk pembangunan dan pelayanan publik yang lebh baik </p>
                            <a href="/login" class="btn btn-secondary mt-3">Adukan Sekarang</a>
                        </div>
                    </div> --}}
                </div>

            </div>
        </section>
        <section class="our-process" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6" data-aos="fade-up">
                        <h5 class="text-dark">Apa itu PINTER?</h5>
                        <h3 class="font-weight-medium text-dark">Pelayanan Administrasi Sistem Elektronik</h3>
                        <h5 class="text-dark mb-3">Dinas Komunikasi dan Informatika Kabupaten Sukoharjo</h5>
                        <p class="font-weight-medium mb-4">Menyediakan berbagai Layanan Administrasi Sistem
                            Elektronik,<br> diantaranya sebagai berikut:
                        </p>
                        <div class="d-flex justify-content-start mb-3">
                            <img src="{{ asset('assets/images/tick.png') }}" alt="tick" class="mr-3 tick-icon">
                            <p class="mb-0">Rekomendasi</p>
                        </div>
                        <div class="d-flex justify-content-start mb-3">
                            <img src="{{ asset('assets/images/tick.png') }}" alt="tick" class="mr-3 tick-icon">
                            <p class="mb-0">Subdomain</p>
                        </div>
                        <div class="d-flex justify-content-start mb-3">
                            <img src="{{ asset('assets/images/tick.png') }}" alt="tick" class="mr-3 tick-icon">
                            <p class="mb-0">Hosting</p>
                        </div>
                        <div class="d-flex justify-content-start mb-3">
                            <img src="{{ asset('assets/images/tick.png') }}" alt="tick" class="mr-3 tick-icon">
                            <p class="mb-0">Pentest (Penetration Testing)</p>
                        </div>
                        <div class="d-flex justify-content-start mb-3">
                            <img src="{{ asset('assets/images/tick.png') }}" alt="tick" class="mr-3 tick-icon">
                            <p class="mb-0">Email</p>
                        </div>
                        <div class="d-flex justify-content-start mb-3">
                            <img src="{{ asset('assets/images/tick.png') }}" alt="tick" class="mr-3 tick-icon">
                            <p class="mb-0">VPN (Virtual Private Network)</p>
                        </div>
                        <div class="d-flex justify-content-start mb-3">
                            <img src="{{ asset('assets/images/tick.png') }}" alt="tick" class="mr-3 tick-icon">
                            <p class="mb-0">Suko Drive</p>
                        </div>
                        <div class="d-flex justify-content-start mb-3">
                            <img src="{{ asset('assets/images/tick.png') }}" alt="tick" class="mr-3 tick-icon">
                            <p class="mb-0">TTE (Tanda Tangan Elektronik)</p>
                        </div>
                    </div>
                    <div class="col-sm-6 text-right" data-aos="flip-left" data-aos-easing="ease-out-cubic"
                        data-aos-duration="2000">
                        <img src="{{ asset('assets/images/idea.png') }}" alt="idea" class="img-fluid">
                    </div>
                </div>
            </div>
        </section>
        <section class="contactus" id="contact">
            <div class="container">
                <div class="row mb-5 pb-5">
                    <div class="col-sm-5" data-aos="fade-up" data-aos-offset="-500">
                        <img src="{{ asset('assets/images/contact.jpg') }}" alt="contact" class="img-fluid">
                    </div>
                    <div class="col-sm-7" data-aos="fade-up" data-aos-offset="-500">
                        <h3 class="font-weight-medium text-dark mt-5 mt-lg-0">Ada Permasalahan</h3>
                        <h5 class="text-dark mb-5">Silahkan isi data serta tulis permasalahan anda</h5>
                        <form>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="name"
                                            placeholder="Nama Lengkap*">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="mail"
                                            placeholder="Email*">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <textarea name="message" id="message" class="form-control" placeholder="Permasalahan*" rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <a href="#" class="btn btn-secondary">Kirim</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <footer class="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <address>
                            <a href="#" class="footer-link">
                                <h7 class="footer-title font-weight-bold">
                                    Dinas Komunikasi dan Informatika
                                </h7>
                                <h6 class="footer-title font-weight-bold">
                                    Kabupaten Sukoharjo
                                </h6>
                            </a>
                            <p class="mb-4 mr-5">Gedung Terpadu Menara Wijaya Lantai 5, Jalan Jenderal Sudirman Nomor
                                199
                                Sukoharjo
                            </p>
                            <div class="d-flex align-items-center">
                                <p class="mr-2 mb-0">Telepon</p>
                                <p class="mr-2 mb-0">: 0271 593068 ext. (1522) Fax 0271 593335</p>
                            </div>
                            <div class="d-flex align-items-center">
                                <p class="mr-4 mb-0">Email</p>
                                <a href="#" class="footer-link">: diskominfo@sukoharjokab.go.id</a>
                            </div>
                        </address>
                        <div class="social-icons">
                            <h6 class="footer-title font-weight-bold">
                                Media Sosial
                            </h6>
                            <div class="d-flex">
                                <a href="#"><i class="mdi mdi-instagram"></i></a>
                                <a href="#"><i class="mdi mdi-facebook-box"></i></a>
                                <a href="#"><i class="mdi mdi-twitter"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-4">
                                <h6 class="footer-title">Tentang</h6>
                                <ul class="list-footer">
                                    <a href="#about" class="footer-link">PINTER</a>
                                </ul>
                            </div>
                            <div class="col-sm-4">
                                <h6 class="footer-title">Layanan</h6>
                                <ul class="list-footer">
                                    <li><a href="#services" class="footer-link">Rekomendasi</a></li>
                                    <li><a href="#services" class="footer-link">Subdomain</a></li>
                                    <li><a href="#services" class="footer-link">Hosting</a></li>
                                    <li><a href="#services" class="footer-link">Pentest</a></li>
                                    <li><a href="#services" class="footer-link">Email</a></li>
                                    <li><a href="#services" class="footer-link">VPN</a></li>
                                    <li><a href="#services" class="footer-link">Suko Drive</a></li>
                                    <li><a href="#services" class="footer-link">Tanda Tangan Elektronik</a></li>
                                </ul>
                            </div>
                            <div class="col-sm-4">
                                <h6 class="footer-title">Link Terkait</h6>
                                <ul class="list-footer">
                                    <li><a href="#" class="footer-link">Pemkab Sukoharjo</a></li>
                                    <li><a href="#" class="footer-link">Diskominfo Sukoharjo</a></li>
                                    <li><a href="#" class="footer-link">PPID Utama</a></li>
                                    <li><a href="#" class="footer-link">PPID Diskominfo</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-flex align-items-center">
                        <img style="width: 10%" src="{{ asset('assets/images/logo/horizontal.png') }}"
                            alt="logo" class="mr-3">
                        <p class="mb-0 text-small pt-1">© Diskominfo Sukoharjo 2022. Design by <a
                                href="https://www.bootstrapdash.com" class="text-white"
                                target="_blank">BootstrapDash</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="{{ asset('assets/vendors/base/vendor.bundle.base.js') }}"></script>
    <script src="{{ asset('assets/vendors/owl.carousel/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('assets/vendors/aos/js/aos.js') }}"></script>
    <script src="{{ asset('assets/vendors/jquery-flipster/js/jquery.flipster.min.js') }}"></script>
    <script src="{{ asset('assets/js/template.js') }}"></script>
</body>

</html>
