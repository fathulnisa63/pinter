<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OpdController;
use App\Http\Controllers\VpnController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\KartuController;
use App\Http\Controllers\RuangController;
use App\Http\Controllers\HostingController;
use App\Http\Controllers\HakaksesController;
use App\Http\Controllers\AktivitasController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\JenisaksesController;
use App\Http\Controllers\JenisserverController;
use App\Http\Controllers\JenispermohonanController;







/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('landingpage');
});

Route::get('/login', function () {
    return view('auth.login');
})->name('login');

Route::middleware(['auth'])->controller(DashboardController::class)->group(function () {
    Route::get('/dashboard', 'index');
});

Route::middleware(['auth'])->controller(OpdController::class)->group(function () {
    Route::get('/opd', 'index');
    Route::get('/opd/add', 'add');
    Route::post('/opd/create', 'create');
    Route::get('/opd/detail/{opd}', 'detail');
    Route::get('/opd/edit/{opd}', 'edit');
    Route::put('/opd/update/{opd}', 'update');
    Route::get('/opd/delete/{opd}', 'delete');
});

Route::middleware(['auth'])->controller(UserController::class)->group(function () {
    Route::get('/opd/{opd}/user/add', 'add');
    Route::post('/opd/user/create', 'create');
    Route::get('/opd/{opd}/user/edit/{user}', 'edit');
    Route::put('/opd/user/update/{user}', 'update');
    Route::get('/opd/user/delete/{user}', 'delete');
    Route::get('/profile', 'profile');
    Route::put('/profile/update/{id}', 'updateProfile');
});

Route::middleware(['auth'])->controller(EmailController::class)->group(function () {
    Route::get('/email', 'index');
    Route::get('/email/baru', 'baru');
    Route::post('/email/create-baru', 'createBaru');
    Route::get('/email/edit/{email}', 'edit');
    Route::put('/email/update/{email}', 'update');
    Route::get('/email/cetak/{email}', 'cetak');
    Route::put('/email/upload/{email}', 'uploadFormulir');
    Route::put('/email/status/{email}', 'status');
    Route::get('/email/perubahan/{email}', 'perubahan');
    Route::post('/email/create-perubahan/{email}', 'createPerubahan');
    Route::get('/email/gantipass/{email}', 'gantipass');
    Route::post('/email/create-gantipass/{email}', 'createGantipass');
    Route::get('/email/delete/{email}', 'delete');
});

Route::middleware(['auth'])->controller(VpnController::class)->group(function () {
    Route::get('/vpn', 'index');
    Route::get('/vpn/baru', 'baru');
    Route::post('/vpn/create-baru', 'createBaru');
    Route::get('/vpn/edit/{vpn}', 'edit');
    Route::put('/vpn/update/{vpn}', 'update');
    Route::get('/vpn/cetak/{vpn}', 'cetak');
    Route::put('/vpn/upload/{vpn}', 'uploadFormulir');
    Route::put('/vpn/status/{vpn}', 'status');
    Route::get('/vpn/perubahan/{vpn}', 'perubahan');
    Route::post('/vpn/create-perubahan/{vpn}', 'createPerubahan');
    Route::get('/vpn/gantipass/{vpn}', 'gantipass');
    Route::post('/vpn/create-gantipass/{vpn}', 'createGantipass');
    Route::get('/vpn/delete/{vpn}', 'delete');
});

Route::middleware(['auth'])->controller(RuangController::class)->group(function () {
    Route::get('/ruangserver', 'index');
    Route::get('/ruangserver/add', 'add');
    Route::post('/ruangserver/store', 'create');
    Route::get('/ruangserver/edit/{ruangserver}', 'edit');
    Route::get('/ruangserver/proses/{id}', 'proses');
    Route::post('/ruangserver/proses/update/{id}', 'prosesUpdate');
    Route::put('/ruangserver/update/{ruangserver}', 'update');
    Route::get('/ruangserver/cetak/{id}', 'cetak');
    Route::put('/ruangserver/upload/{ruangserver}', 'uploadFormulir');
    Route::post('/ruangserver/status/{ruangserver}', 'status');
    Route::get('/ruangserver/delete/{ruangserver}', 'delete');
    Route::get('/ruangserver/detail/{id}', 'detail');
});

Route::middleware(['auth'])->controller(HostingController::class)->group(function () {
    Route::get('/hosting', 'index');
    Route::get('/hosting/create', 'create');
    Route::post('/hosting/store', 'store');
    Route::get('/hosting/edit/{id}', 'edit');
    Route::post('/hosting/update/{id}', 'update');
    Route::get('/hosting/detail/{id}', 'detail');
    Route::get('/hosting/penambahan/create/{id}', 'CreatePenambahan');
    Route::post('/hosting-update/{id}', 'update');
    Route::get('/hosting/delete/{id}', 'delete');
    Route::post('/spek-update/{id}', 'spekUpdate');
    Route::get('/hosting/perubahan/create/{id}', 'CreatePerubahan');
    Route::post('/hosting/perubahan/store/{id}', 'perubahanUpdate');
    Route::get('/hosting/penambahan/create/{id}', 'CreatePenambahan');
    Route::post('/hosting/penambahan/store/{id}', 'penambahanUpdate');
    Route::post('/hosting/upload-formulir/{id}', 'FormBaru');
    Route::post('/hosting/status/{id}', 'PerubahanStatus');
    Route::get('/hosting/cetak/{id}', 'cetakHosting');
});

Route::middleware(['auth'])->controller(HakaksesController::class)->group(function () {
    Route::get('/hakakses', 'index');
    Route::get('/hakakses/add', 'add');
    Route::post('/hakakses/create', 'create');
    Route::get('/hakakses/edit/{hakakses}', 'edit');
    Route::put('/hakakses/update/{hakakses}', 'update');
    Route::get('/hakakses/cetak/{hakakses}', 'cetak');
    Route::put('/hakakses/upload/{hakakses}', 'uploadFormulir');
    Route::put('/hakakses/status/{hakakses}', 'status');
    Route::get('/hakakses/delete/{hakakses}', 'delete');
});

Route::middleware(['auth'])->controller(JenisaksesController::class)->group(function () {
    Route::get('/manajemen-akses', 'index');
    Route::post('/manajemen-akses/create', 'create');
    Route::put('/manajemen-akses/update/{id}', 'update');
    Route::get('/manajemen-akses/delete/{id}', 'delete');
});

// Route::middleware(['auth'])->controller(JenisserverController::class)->group(function () {
//     Route::get('/jenisserver', 'index');
// });

// Route::middleware(['auth'])->controller(JenispermohonanController::class)->group(function () {
//     Route::get('/jenispermohonan', 'index');
//     Route::post('/jenpermohonan-create', 'create');
//     Route::put('/jenpermohonan-update/{id}', 'update');
//     Route::get('/jenpermohonan-delete/{id}', 'delete');
// });

// Route::middleware(['auth'])->controller(HakaksesController::class)->group(function () {
//     Route::get('/permohonanakses', 'index');
// });

// Route::middleware(['auth'])->controller(JenisaksesController::class)->group(function () {
//     Route::get('/jenisakses', 'index');
//     Route::post('/Tambah-akses', 'CreateAkses');
//     Route::post('/Update-akses/{id}', 'UpdateAkses');
//     Route::get('/Delete-akses/{id}', 'DeleteAkses');
// });

// Route::middleware(['auth'])->controller(RuangController::class)->group(function () {
//     Route::get('/ruangserver', 'index');
//     Route::post('/Tambah-ruang', 'CreateRuang');
//     Route::post('/Update-ruang/{id}', 'UpdateRuang');
//     Route::get('/Delete-ruang/{id}', 'DeleteRuang');
// });

// Route::middleware(['auth'])->controller(PrintController::class)->group(function () {
//     // Route::get('/cetakhosting/{id}', 'cetakHosting');
//     // Route::get('/cetakhosting', 'cetakHosting');
//     Route::get('/hosting/cetak/{id}', 'cetakHosting');
//     // Route::get('/cetakhosting', 'cetakHosting');
//     Route::get('/cetakakses','cetakAkses');
//     Route::get('/cetakruang','cetakRuang');
// });

Route::get('/Ruang/detailruang', function () {
    return view('Ruang.detailruang');
});

Route::get('/Hakakses/detailakses', function () {
    return view('Hakakses.detailakses');
});